$(function(){ // on dom ready

let exemples = [
  {
    nodes: [
      { group: 'nodes', data: { id: '4', name: 'Page E', "position": { "x": 400, "y": 400}} },
      { group: 'nodes', data: { id: '6', name: 'Page G', "position": { "x": 600, "y": 400}} },
      { group: 'nodes', data: { id: '1', name: 'Page B', "position": { "x": 400, "y": 600}} },
      { group: 'nodes', data: { id: '7', name: 'Page H', "position": { "x": 600, "y": 600}} },
      { group: 'nodes', data: { id: '0', name: 'Page A', "position": { "x": 500, "y": 700}} },
      { group: 'nodes', data: { id: '5', name: 'Page F', "position": { "x": 400, "y": 800}} },
      { group: 'nodes', data: { id: '2', name: 'Page C', "position": { "x": 600, "y": 800}} },
      { group: 'nodes', data: { id: '3', name: 'Page D', "position": { "x": 500, "y": 900}} }
    ],
    edges: [
      { group: 'edges', data: { source: '0', target: '3' } },
      { group: 'edges', data: { source: '0', target: '5' } },
      { group: 'edges', data: { source: '1', target: '0' } },
      { group: 'edges', data: { source: '1', target: '4' } },
      { group: 'edges', data: { source: '1', target: '6' } },
      { group: 'edges', data: { source: '2', target: '0' } },
      { group: 'edges', data: { source: '2', target: '3' } },
      { group: 'edges', data: { source: '3', target: '2' } },
      { group: 'edges', data: { source: '3', target: '5' } },
      { group: 'edges', data: { source: '4', target: '0' } },
      { group: 'edges', data: { source: '4', target: '6' } },
      { group: 'edges', data: { source: '4', target: '7' } },
      { group: 'edges', data: { source: '5', target: '1' } },
      { group: 'edges', data: { source: '5', target: '2' } },
      { group: 'edges', data: { source: '5', target: '3' } },
      { group: 'edges', data: { source: '6', target: '0' } },
      { group: 'edges', data: { source: '6', target: '1' } },
      { group: 'edges', data: { source: '7', target: '0' } },
      { group: 'edges', data: { source: '7', target: '2' } },
      { group: 'edges', data: { source: '7', target: '6' } }
    ]
    },{
nodes: [
      { group: 'nodes', data: { id: '0', name: 'Page A', "position": { "x": 400, "y": 400}} },
      { group: 'nodes', data: { id: '1', name: 'Page B', "position": { "x": 600, "y": 400}} },
      { group: 'nodes', data: { id: '2', name: 'Page C', "position": { "x": 400, "y": 600}} },],
    edges: [
      { group: 'edges', data: { source: '0', target: '1' } },
      { group: 'edges', data: { source: '0', target: '2' } },
      { group: 'edges', data: { source: '1', target: '0' } },
      { group: 'edges', data: { source: '2', target: '0' } }]
    },
    {
      nodes: [
        { group: 'nodes', data: { id: '0', name: 'Page A', "position": { "x": 400, "y": 400}} },
        { group: 'nodes', data: { id: '1', name: 'Page B', "position": { "x": 600, "y": 400}} },
        { group: 'nodes', data: { id: '2', name: 'Page C', "position": { "x": 400, "y": 600}} },
        { group: 'nodes', data: { id: '3', name: 'Page D', "position": { "x": 600, "y": 600}} },
      ],
      edges: [
        { group: 'edges', data: { source: '0', target: '1' } },
        { group: 'edges', data: { source: '0', target: '2' } },
        { group: 'edges', data: { source: '1', target: '1' } },
        { group: 'edges', data: { source: '2', target: '0' } },
        { group: 'edges', data: { source: '2', target: '1' } },
        { group: 'edges', data: { source: '2', target: '3' } },]
      },
    {
      nodes: [
        { group: 'nodes', data: { id: '0', name: 'Page A', "position": { "x": 400, "y": 400}} },
        { group: 'nodes', data: { id: '1', name: 'Page B', "position": { "x": 600, "y": 400}} },
        { group: 'nodes', data: { id: '2', name: 'Page C', "position": { "x": 400, "y": 600}} },
        { group: 'nodes', data: { id: '3', name: 'Page D', "position": { "x": 600, "y": 600}} },
      ],
      edges: [
        { group: 'edges', data: { source: '0', target: '1' } },
        { group: 'edges', data: { source: '0', target: '2' } },
        { group: 'edges', data: { source: '1', target: '0' } },
        { group: 'edges', data: { source: '2', target: '1' } },
        { group: 'edges', data: { source: '1', target: '2' } },
        { group: 'edges', data: { source: '3', target: '1' } },]
      },
      {
      nodes: [
        { group: 'nodes', data: { id: '0', name: 'Page A', "position": { "x": 400, "y": 400}} },
        { group: 'nodes', data: { id: '1', name: 'Page B', "position": { "x": 600, "y": 400}} },
        { group: 'nodes', data: { id: '2', name: 'Page C', "position": { "x": 400, "y": 600}} },
        { group: 'nodes', data: { id: '3', name: 'Page D', "position": { "x": 600, "y": 600}} },
      ],
      edges: [
        { group: 'edges', data: { source: '0', target: '3' } },
        { group: 'edges', data: { source: '0', target: '2' } },
        { group: 'edges', data: { source: '1', target: '0' } },
        { group: 'edges', data: { source: '3', target: '1' } },
        { group: 'edges', data: { source: '3', target: '2' } },]
      }
];
$('#cy').cytoscape({
  layout: {
    name: 'preset',
    positions : function (node){
      return node._private.data.position;
    }
  },
  style: cytoscape.stylesheet()
    .selector('node')
      .css({
        'shape': 'circle',
        'width': '70',
        'height': '70',
        'content': 'data(name)',
        'text-valign': 'center',
        'background-color': '#FFFF99',
        'border-color': 'black',
        'border-width': '2',
        'text-wrap': 'wrap',
      })
    .selector('edge')
      .css({
        'target-arrow-shape': 'triangle',
        'width': '5',
        'line-color': 'black',
        'color': 'black',
				'target-arrow-color': 'black',
				'curve-style': 'bezier'
      })
    .selector(':selected')
      .css({
        'line-color': 'blue',
        'target-arrow-color': 'blue',
        'source-arrow-color': 'blue'
      })
    .selector('.faded')
      .css({
        'opacity': 0.25,
        'text-opacity': 0
      }),
  elements: exemples[0],

  // on graph initial layout done (could be async depending on layout...)
  ready: function(){
    window.cy = this;

    // giddy up...
    
    cy.elements().unselectify();
    
    cy.selectedNode = null;
    
    cy.on('tap', 'node', function(e){
      var node = e.cyTarget;
      if(cy.selectedNode !== null && cy.selectedNode !== node) {
        cy.add({
            group: "edges",
            data: { target: node.id(), source: cy.selectedNode.id()  },
        });
        cy.elements().removeClass('faded');
        cy.selectedNode = null;
      } else {
        cy.elements().addClass('faded');
        node.removeClass('faded');
        node.select();
        cy.selectedNode = node;
      }
    
    });
    
    cy.on('tap', function(e){
      if( e.cyTarget === cy ){
        cy.elements().removeClass('faded');
        cy.selectedNode = null;
      }
    });
  }
});

function deleteSelected() {
  var selection = cy.$(':selected');
  var edgesToRemove = selection.connectedEdges();
  edgesToRemove.remove();
  selection.remove();
  cy.selectedNode = null;
  cy.elements().removeClass('faded');
}

$('#addPage').on('click', function(e) {
  var newNode = cy.add({
      group: "nodes",
      data: { id: '' + nextID, name: 'Page ' + nextID },
      position: { x: (100 + nextID * 90 % 600), y: 100 }
  });
  nextID++;
});

function pageRank() {
  var pr = cy.elements().pageRank({'dampingFactor': 0.825, 'dampingfactor': 0.825});
  cy.nodes().each( function(i, ele) {
    var nodeRank = pr.rank(ele);
    ele.style({'content': 'Page ' + String.fromCharCode(65 + parseInt(ele.id())) + '\n' + Math.round(nodeRank * 1000) / 1000 });
  });
  cy.nodes().flashClass('nolabel',1);  //force redraw - see http://stackoverflow.com/questions/26123468/dynamic-node-content-label-in-cytoscape-js
}

$('#runPageRank').on('click', function(e) {
  let steps = parseInt(document.querySelector("#steps").value, 10) - 1;
  let nodes = cy.nodes();
  const choix = document.querySelector("#start").selectedOptions[0].value;
  const algo = document.querySelector("#algo").selectedOptions[0].value;
  let current = nodes[(choix == "")?Math.floor(Math.random() * nodes.length):parseInt(choix, 10)];
  let visites = Array(nodes.length).fill(0);
  visites[current._private.data.id]++;
  console.log(current._private.data.name);
  for (let i = 0; i < steps; i++){
    let voisins = cy.$('#'+ current._private.data.id).outgoers().nodes();
    if (voisins.length == 0){
      if (algo == 1 || (algo == 2 && Math.random() <= 0.85)){
        availables = nodes.filter( node => {
          return nodes[node]._private.data.id != current._private.data.id;
        });
        current = availables[Math.floor(Math.random() * availables.length)];
      }
    } else if (voisins.length == 1){
      if (voisins[0]._private.data.id != current._private.data.id){
        current = voisins[Math.floor(Math.random() * voisins.length)];
      } else if (algo == 0 || (algo == 2 && Math.random() <= 0.85)){
        current = nodes[Math.floor(Math.random() * nodes.length)];
      } else {
        availables = nodes.filter( node => {
          return nodes[node]._private.data.id != current._private.data.id;
        });
        current = availables[Math.floor(Math.random() * availables.length)];
      }
    } else {
      if (algo == 0 || algo == 1 || (algo == 2 && Math.random() <= 0.85)){
        current = voisins[Math.floor(Math.random() * voisins.length)];
      }
      else {
        availables = nodes.filter( node => {
          return nodes[node]._private.data.id != current._private.data.id;
        });
        current = availables[Math.floor(Math.random() * availables.length)];
      }
    }
    console.log(current._private.data.name);
    visites[current._private.data.id]++;
  }
  let table = document.querySelector("#visites");
  let colonnes = nodes.map(node => "<td></td>").join('');
  table.innerHTML = "<tr><th>Page</th>" + colonnes + "</tr><tr><th>Nb visites</th>" + colonnes + "</tr><tr><th>PageRank<br/>(Fréquence de visites)</th>" + colonnes + "</tr>";
  for (let i = 0; i < nodes.length; i++){
    table.children[0].children[0].children[i + 1].innerHTML = "<b>" + String.fromCharCode(65 + i) + "</b>";
    table.children[0].children[1].children[i + 1].innerHTML = visites[i];
    table.children[0].children[2].children[i + 1].innerHTML = (visites[i] / (steps + 1)).toFixed(2);
  }
});

document.querySelector("#exemples").addEventListener('change', function(){
  let n = cy.nodes().length;

  for (let i = 0; i < n; i++){
    cy.remove("#" + i);
  }

  let elements = exemples[parseInt(this.value, 10)];
  cy.add(elements.nodes);
  cy.add(elements.edges);
  cy.layout({
    name: 'preset',
    positions : function (node){
      return node._private.data.position;
    }
  });

  let selecteur = document.querySelector("#start");
  selecteur.innerHTML = '';
  let option = document.createElement('option');
  option.value = "";
  option.label = "Aléatoire";
  selecteur.appendChild(option);
  elements.nodes.forEach((item, i) => {
    option = document.createElement('option');
    option.value = i;
    option.label = item.data.name;
    selecteur.appendChild(option);
  });

});

}); // on dom ready
